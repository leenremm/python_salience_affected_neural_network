# ================================================================

'''
# Author: LA Remmelzwaal
# Date: January 2020
# Adapted from: https://gitlab.com/ja4nm/python-neural-net/

Features added:

    >>> Minor bug fixes
    >>> 4 datasets available (xor, cifar10, mnist, fashion_mnist)
    >>> All outputs converted to categorical
    >>> Training progress shown
    >>> Accuracy score shown during training
    >>> K-fold training
'''

# ================================================================

dataset = "fashion_mnist"     # Select dataset: mnist, fashion_mnist
method = "load"       # Method: train / load
kfolds = 5            # K-folds
input_dim = 16        # Image dimensions = input_dim * input_dim
epochs = 10           # Epochs
samples = 1000        # Total image samples used from datasets
loop_samples = 800    # Total samples tagged with salience (loop)

# ================================================================

# imports
from NeuralNetwork import *
import numpy as np
from sklearn.model_selection import StratifiedKFold

# ================================================================

# setup dataset
if (dataset in ["mnist"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
    learning_rate = 0.1
elif (dataset in ["fashion_mnist"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
    learning_rate = 0.1
elif (dataset in ["cifar10"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
    learning_rate = 0.1
else:
    quit()

# Looping through each samples
i_specific_image = []
i_same_class = []
i_diff_class = []
i_entire_set = []

# K Fold Validation Testing
kfold = StratifiedKFold(n_splits=kfolds, shuffle=True, random_state=1)
kfold_error = []
kfold_acc = []
for k, (train_index, val_index) in enumerate(kfold.split(x_train, y_train.argmax(1))):

    print ("\n========================\nK-fold: %d / %d\n========================" % (k+1,kfolds))

    # k-fold datasets
    x_train_fold, x_test_fold = x_train[train_index], x_train[val_index]
    y_train_fold, y_test_fold = y_train[train_index], y_train[val_index]
    y_train_fold_argmax = y_train_fold.argmax(1)

    # Visualize images
    #NeuralNetwork.show_100_images(x_train_fold)

    # setup NN Model
    nn_dim = [input_dim*input_dim, input_dim*input_dim, 10]
    net = NeuralNetwork(nn_dim)
    net.set_speed(learning_rate)

    # optional settings
    net.set_init_method(net.init_rand)
    net.set_activation_function(net.activation_sigmoid)

    # Print to screen
    print ("\nNeural Network dimensions: %s" % nn_dim)
    print ("Dataset: %s" % dataset)
    print ("Image Dimensions: %d x %d px" % (input_dim,input_dim))
    print ("Training set size: %d" % len(x_train_fold))
    print ("Testing set size: %d" % len(x_test_fold))
    print ("Epochs: %d" % epochs)
    print ("Method: %s" % method)
    print ("Learning Rate (0-1): %5.4f\n" % net.get_speed())

    # ==========================================================================

    # Loop through all elements in the dataset to get average response
    for i in range (0, loop_samples):

        loop_sample_x = x_train_fold[i]
        loop_sample_y = y_train_fold_argmax[i]

        # ==========================================================================

        filename = "saved_models/model_%s_e%d_k%d.bin" % (dataset, epochs, k+1)
        if (method == "train"):

            # train
            #print("Training...")
            net.train(x_train_fold, y_train_fold, x_test_fold, y_test_fold, epochs)
            net.save_to_bin(filename)

        else:

            # load from file
            #print ("\nLoading model from BIN file...")
            net = NeuralNetwork.load_from_bin(filename)
            #net.draw()

        # ==========================================================================

        # Apply Salience (once off)
        net.initialize_salience()
        net.salience_train_activation(x_train_fold[i], salience_value=1)
        #net.salience_train_activation(x_train_fold[i+1], salience_value=-1)
        #net.draw()
        #import pdb; pdb.set_trace()

        # ==========================================================================

        # What does the NN think the class is?
        nn_y = np.argmax(net.solve(np.array([loop_sample_x])), axis=1)[0]

        # ==========================================================================

        print ("K-fold: %d/%d, Sample #: %3d/%3d, Training Class: %d, NN Predicted Class: %d, Correct: %s" % (k+1, kfolds, i, loop_samples, loop_sample_y, nn_y, (nn_y == loop_sample_y)))

        # ==========================================================================

        # Calculate Reverse Salience (across all samples)
        k_reverse_salience_all = []
        k_reverse_salience_classes = [[],[],[],[],[],[],[],[],[],[]]
        k_reverse_salience_same_class = []
        k_reverse_salience_diff_class = []
        k_reverse_salience_specific = []

        for j in range(0,len(x_train_fold)):

            # Training Class
            j_class = y_train_fold_argmax[i]

            # NN Class
            j_class_nn = np.argmax(net.solve(np.array([x_train_fold[j]])), axis=1)[0]

            # Reverse Salience
            j_rev_salience = net.calculate_reverse_salience(x_train_fold[j])

            # Append: ALL
            k_reverse_salience_all.append(j_rev_salience)

            # Append: Split into classes
            k_reverse_salience_classes[j_class_nn].append(j_rev_salience)

            # Append: Same / Different class
            if (nn_y == j_class_nn):
                k_reverse_salience_same_class.append(j_rev_salience)
            else:
                k_reverse_salience_diff_class.append(j_rev_salience)

            # Append: Specific tagged image
            if (i == j):
                k_reverse_salience_specific.append(j_rev_salience)

        # Print class breakdown
        #for p in range(0,len(class_reverse_salience)):
            #print ("%d, %.5f" % (p, class_reverse_salience[p]))

        # Calculate averages
        k_var_reverse_salence = [np.average(m) for m in k_reverse_salience_classes]
        k_var_specific = np.average(k_reverse_salience_specific)
        k_var_same = np.average(k_reverse_salience_same_class)
        k_var_diff = np.average(k_reverse_salience_diff_class)
        k_var_all = np.average(k_reverse_salience_all)

        # Add to loop arrays
        i_specific_image.append(k_var_specific)
        i_same_class.append(k_var_same)
        i_diff_class.append(k_var_diff)
        i_entire_set.append(k_var_all)

        # ==========================================================================

    #print ("\nError/Accuracy Results:\n")
    error = net.get_avg_error()
    kfold_error.append(error)
    #print("\nNN Error: %.5f" % (error))
    acc = net.evaluate(x_test_fold, y_test_fold)
    kfold_acc.append(acc)
    #print("NN Accuracy: %5.2f%%" % (acc * 100))

print ("\n========================\nReverse Salience Results:\n========================\n")
print ("i_specific_image: %.5f +- %.5f" % (np.average(i_specific_image), np.std(i_specific_image)))
print ("i_same_class:     %.5f +- %.5f" % (np.average(i_same_class), np.std(i_same_class)))
print ("i_diff_class:     %.5f +- %.5f" % (np.average(i_diff_class), np.std(i_diff_class)))
print ("i_entire_set:     %.5f +- %.5f" % (np.average(i_entire_set), np.std(i_entire_set)))

print ("\n========================\nK-Fold Summary:\n========================\n")
print("K-fold Accuracy: %5.2f%% +- %3.2f%%" % (np.mean(kfold_acc) * 100, np.std(kfold_acc) * 100))
print("K-fold Error:    %.3f +- %.3f" % (np.mean(kfold_error), np.std(kfold_error)))

print ("\n========================\nEnd of script!\n========================\n")
import pdb; pdb.set_trace()
