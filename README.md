# Salience Affected Neural Network

## About

Author: LA Remmelzwaal

Written: January 2020

License: CC BY NC

Pre-print Paper: https://arxiv.org/abs/1908.03532

## Description

A Python implementation of a Salience Affected Neural Network

## Software Installation

  * Install Python 3.6.4 or later: (e.g. https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) (25Mb)

## Virtual Environment Setup

  * Open Miniconda / Anaconda / MS Command Prompt
  * Run: `venv_setup.bat` to setup Virtual Env with PIP dependencies. This is only required once.

## Running the code

  * Activate virtual environment `venv_activate.bat`
  * Run: `python main.py`

## Example CMD output

![Example CMD Output](https://bitbucket.org/leenremm/python_neural_network/raw/ca841852e9df4dcb77e1fa86eea1414acd46a3d1/img/example_cmd_output.png "Example CMD Output")
