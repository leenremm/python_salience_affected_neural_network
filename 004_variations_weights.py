# ================================================================

'''
# Author: LA Remmelzwaal
# Date: January 2020
# Adapted from: https://gitlab.com/ja4nm/python-neural-net/

Features added:

    >>> Minor bug fixes
    >>> 4 datasets available (xor, cifar10, mnist, fashion_mnist)
    >>> All outputs converted to categorical
    >>> Training progress shown
    >>> Accuracy score shown during training
    >>> K-fold training
'''

# ================================================================

dataset = "mnist"               # Select dataset: mnist, fashion_mnist
method = "load"                 # Method: train / load
kfolds = 5                      # K-folds
input_dim = 16                  # Image dimensions = input_dim * input_dim
epochs = 10                     # Epochs
samples = 1000                  # Total image samples used from datasets
loop_samples = 800              # Total samples tagged with salience (loop)
variation = "weights"           # standard, weights

# ================================================================

# imports
from NeuralNetwork import *
import numpy as np
from sklearn.model_selection import StratifiedKFold
import copy

# ================================================================

# setup dataset
if (dataset in ["mnist"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
    learning_rate = 0.1
elif (dataset in ["fashion_mnist"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
    learning_rate = 0.1
elif (dataset in ["cifar10"]):
    x_train, y_train = NeuralNetwork.load_dataset(dataset, input_dim=input_dim, samples=samples)
    learning_rate = 0.1
else:
    quit()

# Looping through each samples
i_specific_image = []
i_same_class = []
i_diff_class = []
i_entire_set = []

# K Fold Validation Testing
kfold = StratifiedKFold(n_splits=kfolds, shuffle=True, random_state=1)
kfold_error = []
kfold_acc = []
for k, (train_index, val_index) in enumerate(kfold.split(x_train, y_train.argmax(1))):

    print ("\n========================\nK-fold: %d / %d\n========================" % (k+1,kfolds))

    # k-fold datasets
    x_train_fold, x_test_fold = x_train[train_index], x_train[val_index]
    y_train_fold, y_test_fold = y_train[train_index], y_train[val_index]
    y_train_fold_argmax = y_train_fold.argmax(1)

    # Visualize images
    NeuralNetwork.show_100_images(x_train_fold)

    # setup NN Model
    nn_dim = [input_dim*input_dim, input_dim*input_dim, 10]
    net = NeuralNetwork(nn_dim)
    net.set_speed(learning_rate)

    # optional settings
    net.set_init_method(net.init_rand)
    net.set_activation_function(net.activation_sigmoid)

    # Print to screen
    print ("\nNeural Network dimensions: %s" % nn_dim)
    print ("Dataset: %s" % dataset)
    print ("Image Dimensions: %d x %d px" % (input_dim,input_dim))
    print ("Training set size: %d" % len(x_train_fold))
    print ("Testing set size: %d" % len(x_test_fold))
    print ("Epochs: %d" % epochs)
    print ("Method: %s" % method)
    print ("Learning Rate (0-1): %5.4f\n" % learning_rate)
    print ("Variation: %s\n" % variation)

    # ==========================================================================

    # Loop through all elements in the dataset to get average response
    for i in range (0, loop_samples):

        loop_sample_x = x_train_fold[i]
        loop_sample_y = y_train_fold_argmax[i]

        # ==========================================================================

        filename = "saved_models/model_%s_e%d_k%d.bin" % (dataset, epochs, k+1)
        if (method == "train"):

            # train
            #print("Training...")
            net.initialize_salience()
            net.train(x_train_fold, y_train_fold, x_test_fold, y_test_fold, epochs)
            net.save_to_bin(filename)

        else:

            # load from file
            #print ("\nLoading model from BIN file...")
            net = NeuralNetwork.load_from_bin(filename)
            #net.draw()

        # ==========================================================================

        # Apply Salience (once off)
        net.initialize_salience()
        net.set_variation(variation)
        net.salience_train_activation(x_train_fold[i], salience_value=1)
        #net.draw()
        #import pdb; pdb.set_trace()

        # ==========================================================================


        before_w = copy.deepcopy(net.w)

        # Weights update
        if (net.variation == "weights"):
            net.salience_update_weights(x_train_fold[i])

        after_w = copy.deepcopy(net.w)

        # Draw before weights
        # net.draw(before_w)

        # Draw difference
        # net.draw(before_w, after_w)

        # Draw after weights
        # net.draw(after_w)

        # Weights update

        # ==========================================================================

        # What does the NN think the class is?
        nn_y = np.argmax(net.solve(np.array([loop_sample_x])), axis=1)[0]

        # ==========================================================================

        print ("K-fold: %d/%d, Sample #: %3d/%3d, Training Class: %d, NN Predicted Class: %d, Correct: %s" % (k+1, kfolds, i, loop_samples, loop_sample_y, nn_y, (nn_y == loop_sample_y)))

        # ==========================================================================

        #print ("\nError/Accuracy Results:\n")
        error = net.get_avg_error()
        kfold_error.append(error)
        #print("\nNN Error: %.5f" % (error))
        acc = net.evaluate(x_test_fold, y_test_fold)
        kfold_acc.append(acc)
        #print("NN Accuracy: %5.2f%%" % (acc * 100))


print ("\n========================\nK-Fold Summary:\n========================\n")
print("K-fold Accuracy: %5.2f%% +- %3.2f%%" % (np.mean(kfold_acc) * 100, np.std(kfold_acc) * 100))
print("K-fold Error:    %.3f +- %.3f" % (np.mean(kfold_error), np.std(kfold_error)))

print ("\n========================\nEnd of script!\n========================\n")
import pdb; pdb.set_trace()
