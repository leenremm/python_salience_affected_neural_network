call conda activate project
cls

:: =================================================
:: Install Python Packages and Dependencies
:: =================================================

pip install pandas
pip install Pillow
pip install numpy
pip install scipy
pip install imutils
pip install matplotlib
pip install pydot
pip install scikit-learn
pip install keras
pip install opencv-python
